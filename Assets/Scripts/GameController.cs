﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TikTokLiveSharp.Events.Objects;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public static GameController ins;

    private void Awake()
    {
        if(ins != null)
        {
            Destroy(gameObject);
        }
        else
        {
            ins = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Update()
    {
        StartGameMenu();
        PlayGameMenu();
        TimeEndGame();
    }

    public void StartGameMenu()
    {
        if (DataManager.ins.timeActiveStartGame)
        {
            DataManager.ins.countDownStartGame -= Time.deltaTime;
            if (DataManager.ins.countDownStartGame < 0)
            {
                DataManager.ins.timeActiveStartGame = false;
                UIManager.ins.ShowPlayGame();
            }
        }
    }

    public void PlayGameMenu()
    {
        if(DataManager.ins.timeActivePlayGame)
        {
            DataManager.ins.countDownPlayGame -= Time.deltaTime;
            if(DataManager.ins.countDownPlayGame < 6)
            {
                ((PlayGameUI)UIManager.ins.currentScreen).answerObj[DataManager.ins.correctAnswer].GetComponent<Image>().color = Color.green;
                ((PlayGameUI)UIManager.ins.currentScreen).bxhTop3.SetActive(true);
              
            }
            if (DataManager.ins.countDownPlayGame < 0)
            {
                foreach (PlayerScore player in DataManager.ins.playerScores)
                {
                    if (player.lastChosenAnswer == DataManager.ins.correctAnswer.ToString())
                    {
                        player.questionCorrectConsecutive++;
                        player.questionCorrectCount++;
                    }
                    else if(player.lastChosenAnswer != DataManager.ins.correctAnswer.ToString())
                    {
                        player.questionCorrectConsecutive = 0;
                    }
                }
                ResetPlayerAnswer();
                DataManager.ins.countDownPlayGame = 60;
                ((PlayGameUI)UIManager.ins.currentScreen).clock.GetComponent<Slider>().value = 60;
                ((PlayGameUI)UIManager.ins.currentScreen).bxhTop3.SetActive(false);
                DataManager.ins.questionNumber++;
                ((PlayGameUI)UIManager.ins.currentScreen).UpdateQuestTxt();
                if (DataManager.ins.questionNumber > 10)
                {
                    DataManager.ins.timeActivePlayGame = false;
                    UIManager.ins.ShowEndGame();
                    DataManager.ins.questionNumber = 1; 
                    DataManager.ins.countDownPlayGame = 60;
                }
            }      
        }
    }

    public void TimeEndGame()
    {
        if(DataManager.ins.timeActiveEndGame)
        {
            DataManager.ins.countDownEndGame -= Time.deltaTime;
            if(DataManager.ins.countDownEndGame < 0)
            {
                DataManager.ins.timeActiveEndGame = false;
                UIManager.ins.ShowStarGame();
                DataManager.ins.countDownEndGame = 10;
                DataManager.ins.countDownStartGame = 120;
                ((StartGameUI)UIManager.ins.currentScreen).clock.GetComponent<Slider>().value = 30;
            }
        }
    }


    public int CalculateScore(float timeToAnswers)
    {
        int maxScore = 100;
        double maxTimeSeconds = 60.0;

        double timeInSeconds = timeToAnswers;
        double scoreRatio = 1 - (timeInSeconds / maxTimeSeconds);
        int score = (int)(maxScore * scoreRatio);

        return score;
    }

    public void PlayerAnswered(long playerId, string playerName, string chosenAnswer, string correctAnswer, float timeToAnswer)
    {
        var playerScore = DataManager.ins.playerScores.FirstOrDefault(player => player.playerId == playerId);

        if (playerScore == null)
        {
            playerScore = new PlayerScore
            {
                playerId = playerId,
                playerName = playerName,
                score = 0,
                timeToAnswer = 0,
                questionCorrectConsecutive = 0,
                questionCorrectCount = 0
            };
            DataManager.ins.playerScores.Add(playerScore);
        }

        if (playerScore.lastChosenAnswer != correctAnswer)
        {
            if (chosenAnswer == correctAnswer)
            {
                playerScore.score += CalculateScore(timeToAnswer);
                playerScore.timeToAnswer += timeToAnswer;
                playerScore.timeToAnswerCorrect = timeToAnswer;
                playerScore.inCorrectCount = 0;  
            }
            playerScore.lastChosenAnswer = chosenAnswer;           
        }

        else
        {
            if (chosenAnswer != correctAnswer)
            {
                if (playerScore.inCorrectCount == 0 && playerScore.timeToAnswerCorrect != 0)
                {
                    playerScore.score -= CalculateScore(playerScore.timeToAnswerCorrect);
                    playerScore.timeToAnswer -= playerScore.timeToAnswerCorrect;
                    playerScore.timeToAnswerCorrect = 0;
                    playerScore.inCorrectCount++;
                }
                if(playerScore.inCorrectCount > 1)
                {
                    playerScore.score -= 0;
                    playerScore.timeToAnswer -= 0;
                }
                if(DataManager.ins.countDownPlayGame < 0)
                {
                    playerScore.inCorrectCount = 0;
                }
               
            }
            playerScore.lastChosenAnswer = chosenAnswer;
        }

        DataManager.ins.playerScores.Sort((a, b) =>
        {
            if(a.score == b.score)
            {
                return a.timeToAnswer.CompareTo(b.timeToAnswer);
            }
            return b.score.CompareTo(a.score);
        });
    }


    public string TimeFormatted(float timeToAnswer)
    {
        TimeSpan time = TimeSpan.FromSeconds(timeToAnswer);
        string timeformatted = string.Format("{0:D2}:{1:D2}:{2:D2}", time.Minutes, time.Seconds, time.Milliseconds);
        return timeformatted;
    }

    public void ResetPlayerAnswer()
    {
        DataManager.ins.ansAPlayerCount.Clear();
        DataManager.ins.ansBPlayerCount.Clear();
        DataManager.ins.ansCPlayerCount.Clear();
        DataManager.ins.ansDPlayerCount.Clear();
    }
  
}
