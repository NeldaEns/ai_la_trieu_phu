using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

#if UNITY_EDITOR
public class QuestionWindow : EditorWindow
{
    private int levelIndex = 0;
    [MenuItem("GameData/Question")]
    public static void showwindow()
    {
        QuestionWindow window = GetWindow<QuestionWindow>("LevelConfig");
    }

    private void OnGUI()
    {
        List<QuestionData> questList = Resources.LoadAll<QuestionData>("Data").OrderBy(x => x.id).ToList();

        if(questList.Count > levelIndex)
        {
            QuestionData data = questList[levelIndex];

            GUILayout.BeginHorizontal();

            if (GUILayout.Button("Prev"))
            {
                levelIndex--;
                if(levelIndex < 0)
                {
                    levelIndex = questList.Count - 1;
                }
            }

            GUILayout.Label("Level " + data.id);

            if (GUILayout.Button("Next"))
            {
                levelIndex++;
                if(levelIndex >= questList.Count)
                {
                    levelIndex = 0; 
                }
            }

            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Question Text");
            data.questionText = EditorGUILayout.TextField(data.questionText);
            GUILayout.EndHorizontal();

            GUILayout.Label("Answer");

            GUILayout.BeginVertical();
            for(int i = 0; i < data.answers.Length; i++)
            {
                data.answers[i] = EditorGUILayout.TextField(data.answers[i]);
            }
            GUILayout.EndVertical();

            data.correctAnswer = EditorGUILayout.IntField("Correct Answer",data.correctAnswer);

        }

        if (GUILayout.Button("Save",GUILayout.Width(350), GUILayout.Height(50)))
        {
            EditorUtility.SetDirty(questList[levelIndex]);
            AssetDatabase.SaveAssets();
        }

        if (GUILayout.Button("AddLevel"))
        {
            QuestionData question = CreateInstance<QuestionData>();
            question.id = questList.Count + 1;
            question.answers = new string[4];
            question.correctAnswer = 0;
            AssetDatabase.CreateAsset(question, "Assets/Resources/Data/Question" + (questList.Count + 1) + ".asset");
            AssetDatabase.SaveAssets();
        }
    }
}
#endif
