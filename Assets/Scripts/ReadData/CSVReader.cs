﻿using System.Collections.Generic;
using UnityEngine;

public class CSVReader : MonoBehaviour
{
    public TextAsset excelData;

    private void Start()
    {
        LoadQuestionData();
    }

    void LoadQuestionData()
    {
        string[] lines = excelData.text.Split('\n');
        List<QuestionData> questionsList = new List<QuestionData>();

        for (int i = 1; i < lines.Length; i++)
        {
            if (string.IsNullOrWhiteSpace(lines[i]))
            {
                continue;
            }

            string[] fields = ParseCsvLine(lines[i]);

            if (fields.Length >= 7)
            {
                QuestionData question = ScriptableObject.CreateInstance<QuestionData>();

                question.id = int.TryParse(fields[0], out int idResult) ? idResult : 0;
                question.questionText = fields.Length > 1 ? fields[1] : "";

               
                List<string> answersList = new List<string>();
                for (int j = 2; j < 6; j++)
                {
                    answersList.Add(fields[j]);
                }
                question.answers = answersList.ToArray();

                if (int.TryParse(fields[6], out int correctAnswerResult))
                {
                    question.correctAnswer = correctAnswerResult;
                }
                questionsList.Add(question);
            }
        }

        DataManager.ins.questionsData = questionsList;
    }

    string[] ParseCsvLine(string line)
    {
        List<string> fields = new List<string>();
        bool insideQuotes = false;
        string currentField = "";

        foreach (char c in line)
        {
            if (c == '"')
            {
                insideQuotes = !insideQuotes;
            }
            else if (c == ',' && !insideQuotes)
            {
                fields.Add(currentField.Trim()); 
                currentField = "";
            }
            else
            {
                currentField += c;
            }
        }

        fields.Add(currentField.Trim()); 

        return fields.ToArray();
    }
}