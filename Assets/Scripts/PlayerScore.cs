[System.Serializable]
public class PlayerScore
{
    public long playerId;
    public string playerName;
    public int score;
    public float timeToAnswer;
    public string lastChosenAnswer;
    public float timeToAnswerCorrect;
    public int inCorrectCount;
    public int questionCorrectCount;
    public int winnerGame;
    public int questionCorrectConsecutive;
    public string titleWon;

    public void UpdateLastChosenAnswer(string chosenAnswer)
    {
        lastChosenAnswer = chosenAnswer;
    }
}
