﻿using System.Collections;
using System.Collections.Generic;
using TikTokLiveSharp.Events;
using TikTokLiveSharp.Events.Objects;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    public static DataManager ins;

    [Header("QUESTION")]
    public List<QuestionData> questions;
    public List<QuestionData> questionsData;
    public int questionNumber = 1;

    [Header("ANSWER")]
    public int correctAnswer;
    public List<User> ansAPlayerCount;
    public List<User> ansBPlayerCount;
    public List<User> ansCPlayerCount;
    public List<User> ansDPlayerCount;

    [Header("USER")]
    public List<User> playerCount;
    public List<UserDonateGift> userDonateCount;
    public List<PlayerScore> playerScores;
    public List<GiftMessage> userGift;
    public List<TikTokGift> userGift1;
    public List<Chat> chatUserDonate;
    public List<UserDonateGift> userDonateTitle;
    public List<PlayerScore> playerTitle;
    public Dictionary<long, Color> giftColors;  

    [Header("TIME")]
    public float countDownStartGame = 10;
    public float countDownPlayGame = 10;
    public float countDownEndGame = 5;

    [Header("BOOL")]
    public bool timeActiveStartGame = false;
    public bool timeActivePlayGame;
    public bool timeActiveEndGame;

    [Header("OTHER")]
    public string roomId; 
    public Dictionary<int, string> titleList = new Dictionary<int, string> 
    {
        {1, "Khởi đầu" },
        {2, "Triệu phú nhỏ" },
        {3, "Triệu phú vừa" },
        {4, "Triệu phú bự" },
        {5, "Sáng dạ" },
        {6, "Thông minh" },
        {7, "Thiên tài" },
        {8, "Hiểu biết rộng" },
        {9, "Nhà thông thái" },
        {10, "Hiền triết" },
        {11, "Thần tri thức" },
        {12, "Cổ vũ" },
        {13, "Siêu cổ vũ" },
        {14, "Nhà từ thiện" },
        {15, "Tri thức" },
        {16, "Đứng đầu" },
        {17, "The Top" },
        {18, "The GOAT" },
    };


    private void Awake()
    {
        if(ins != null)
        {
            Destroy(gameObject);
        }
        else
        {
            ins = this;
            DontDestroyOnLoad(gameObject);
        }
    }
}
