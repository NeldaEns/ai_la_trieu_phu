
using System;

[System.Serializable]
public class UserDonateGift 
{
    public long userId;
    public string userName;
    public int numberOfDonations;
    public string titleDonate;
    public long rose;
    public long tiktok;
    public long fingerHeart;
    public long gg;
    public long rosa;
    public long perfume;
    public long donut;
    public string DateTimeNow = DateTime.Now.ToString("HH:mm");
}
