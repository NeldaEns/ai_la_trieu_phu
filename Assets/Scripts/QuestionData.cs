using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/Questions", fileName = "Question.asset")]
public class QuestionData : ScriptableObject
{
    public int id;
    public string questionText;
    public string[] answers;
    public int correctAnswer;
}



