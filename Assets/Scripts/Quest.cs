using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Quests/Question", fileName = "Questiones.asset")]
public class Quest : ScriptableObject
{
    public int id;
    public string questionText;
    public string[] answers;
    public int correctAnswer;

    public void SetQuestionData(int id, string questionText, string[] answers, int correctAnswer)
    {
        this.id = id;
        this.questionText = questionText;
        this.answers = answers;
        this.correctAnswer = correctAnswer;
    }
}

