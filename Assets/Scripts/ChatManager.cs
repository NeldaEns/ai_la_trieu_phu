﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TikTokLiveSharp.Events;
using System.Text;
using System;

public class ChatManager : MonoBehaviour
{
    public Text chatText;
    private int maxChatLines = 5;

    void Update()
    {
        UpdateChatDisplay();
    }

    private void UpdatePlayerTitles()
    {
        foreach (PlayerScore player in DataManager.ins.playerScores)
        {
            switch (player.questionCorrectCount)
            {
                case 1:
                    player.titleWon = DataManager.ins.titleList[1];
                    break;
                case 100:
                    player.titleWon = DataManager.ins.titleList[11];
                    break;
                case 75:
                    player.titleWon = DataManager.ins.titleList[10];
                    break;
                case 50:
                    player.titleWon = DataManager.ins.titleList[9];
                    break;
                case 40:
                    player.titleWon = DataManager.ins.titleList[8];
                    break;
                case 30:
                    player.titleWon = DataManager.ins.titleList[7];
                    break;
                case 20:
                    player.titleWon = DataManager.ins.titleList[6];
                    break;
                case 10:
                    player.titleWon = DataManager.ins.titleList[5];
                    break;
            }

            switch (player.questionCorrectConsecutive)
            {
                case 15:
                    player.titleWon = DataManager.ins.titleList[4];
                    break;
                case 10:
                    player.titleWon = DataManager.ins.titleList[3];
                    break;
                case 5:
                    player.titleWon = DataManager.ins.titleList[2];
                    break;
            }

            switch (player.winnerGame)
            {
                case 10:
                    player.titleWon = DataManager.ins.titleList[18];
                    break;
                case 5:
                    player.titleWon = DataManager.ins.titleList[17];
                    break;
                case 1:
                    player.titleWon = DataManager.ins.titleList[16];
                    break;
            }

            if (player.score == 1000)
            {
                player.titleWon = DataManager.ins.titleList[15];
            }

            if (DataManager.ins.playerTitle.Count == 0)
            {
                DataManager.ins.playerTitle.Add(player);
            }
            else if (DataManager.ins.playerTitle.Count > 0)
            {
                if (!DataManager.ins.playerTitle.Any(user => user.playerId == player.playerId))
                {
                    DataManager.ins.playerTitle.Add(player);
                }
            }
        }
    }    

    private void UpdateUserDonateTitles()
    {
        foreach (UserDonateGift user in DataManager.ins.userDonateCount)
        {
            switch (user.numberOfDonations)
            {
                case 15:
                    user.titleDonate = DataManager.ins.titleList[14];
                    break;
                case 10:
                    user.titleDonate = DataManager.ins.titleList[13];
                    break;
                case 5:
                    user.titleDonate = DataManager.ins.titleList[12];
                    break;
            }

            if (DataManager.ins.userDonateTitle.Count == 0)
            {
                DataManager.ins.userDonateTitle.Add(user);
            }
            else if (DataManager.ins.userDonateTitle.Count > 0)
            {
                if (!DataManager.ins.userDonateTitle.Any(player => player.userId == user.userId))
                {
                    DataManager.ins.userDonateTitle.Add(user);
                }
            }
        }
    }


    private void UpdateChatDisplay()
    {
        UpdateUserDonateTitles();
        UpdatePlayerTitles();
        if (DataManager.ins.userGift.Count > maxChatLines)
        {
            DataManager.ins.userGift.RemoveAt(0);
        }

        if (DataManager.ins.userGift1.Count > maxChatLines)
        {
            DataManager.ins.userGift1.RemoveAt(0);
        }

        if (DataManager.ins.chatUserDonate.Count > maxChatLines)
        {
            DataManager.ins.chatUserDonate.RemoveAt(0);
        }

        if (DataManager.ins.userDonateTitle.Count > maxChatLines)
        {
            DataManager.ins.userDonateTitle.RemoveAt(0);
        }

        if(DataManager.ins.playerTitle.Count > maxChatLines)
        {
            DataManager.ins.playerTitle.RemoveAt(0);
        }

        StringBuilder chatStringBuilder = new StringBuilder();

        AppendChatMessages(DataManager.ins.userGift1, DataManager.ins.userGift, chatStringBuilder);
        AppendUserDonateChatMessages(DataManager.ins.userDonateTitle, DataManager.ins.userDonateCount, DataManager.ins.chatUserDonate, DataManager.ins.playerScores, chatStringBuilder);
        chatText.text = chatStringBuilder.ToString();
    }

    private Color GetChatColor(TikTokLiveSharp.Events.Objects.TikTokGift gift1, GiftMessage gift, UserDonateGift user, PlayerScore player, Chat chat)
    {
        if (gift != null && DataManager.ins.giftColors.ContainsKey(gift.Gift.Id))
        {
            return DataManager.ins.giftColors[gift.Gift.Id];
        }
        else if(gift != null && !DataManager.ins.giftColors.ContainsKey(gift.Gift.Id))
        {
            return new Color(63 / 255f, 243 / 255f, 144 / 255f);
        }

        else if(gift1 != null && DataManager.ins.giftColors.ContainsKey(gift1.Gift.Id))
        {
            return DataManager.ins.giftColors[gift1.Gift.Id];
        }

        else if (gift1 != null && !DataManager.ins.giftColors.ContainsKey(gift1.Gift.Id))
        {
            return new Color(63 / 255f, 243 / 255f, 144 / 255f);
        }

        else if(user != null)
        {
            return new Color(236 / 255f, 170 / 255f, 170 / 255f);
        }

        else if(player != null)
        {
            return new Color(236 / 255f, 170 / 255f, 170 / 255f);
        }

        else if(chat != null)
        {
            if (chat.Sender.Id == user.userId)
            {
                if (user.donut == 20)
                {
                    return DataManager.ins.giftColors[5879];
                }
                else if (user.perfume == 5)
                {
                    return DataManager.ins.giftColors[5658];
                }
                else if (user.rosa == 100)
                {
                    return DataManager.ins.giftColors[8913];
                }
                else if (user.gg == 100)
                {
                    return DataManager.ins.giftColors[6064];
                }
                else if (user.fingerHeart == 100)
                {
                    return DataManager.ins.giftColors[5487];
                }
                else if (user.tiktok == 100)
                {
                    return DataManager.ins.giftColors[5269];
                }
                else if (user.rose == 100)
                {
                    return DataManager.ins.giftColors[5655];
                }
            }
        }

        return new Color(255 / 255f, 255 / 255f, 255 / 255f);
    }

    private void AppendChatMessages(List<TikTokLiveSharp.Events.Objects.TikTokGift> messages1,List<GiftMessage> messages, StringBuilder chatStringBuilder)
    {
        foreach (GiftMessage user in messages)
        {
            Color chatColor = GetChatColor(null ,user, null , null, null);

            chatStringBuilder.Append($"<color=#{ColorUtility.ToHtmlStringRGB(chatColor)}>{user.DateTimeNow}  {user.User.NickName}: vừa donate {user.Amount} {user.Gift.Name}</color>\n");
        }

        foreach (TikTokLiveSharp.Events.Objects.TikTokGift user in messages1)
        {
            Color chatColor = GetChatColor(user, null, null, null, null);

            chatStringBuilder.Append($"<color=#{ColorUtility.ToHtmlStringRGB(chatColor)}>{user.DateTimeNow}  {user.Sender.NickName}: vừa donate {user.Amount} {user.Gift.Name}</color>\n");
        }
    }

    private void AppendUserDonateChatMessages(List<UserDonateGift> userTitles, List<UserDonateGift> userDonates,List<Chat> chatUser, List<PlayerScore> players,StringBuilder chatStringBuilder)
    {
        foreach (Chat chat in chatUser)
        {
            foreach (UserDonateGift user in userDonates)
            {
                if (chat.Sender.Id == user.userId)
                {
                    Color chatColor = GetChatColor(null, null, user, null, chat);

                    chatStringBuilder.Append($"<color=#{ColorUtility.ToHtmlStringRGB(chatColor)}>{chat.DateTimeNow}  {chat.Sender.NickName}: {chat.Message}</color>\n");
                }
            }
        }

        foreach (UserDonateGift user in userTitles)
        {
            if (user.titleDonate == DataManager.ins.titleList[14] || user.titleDonate == DataManager.ins.titleList[13] || user.titleDonate == DataManager.ins.titleList[12])
            {
                Color chatColor = GetChatColor(null, null, user, null, null);

                chatStringBuilder.Append($"<color=#{ColorUtility.ToHtmlStringRGB(chatColor)}>{user.DateTimeNow}  {user.userName}: vừa đạt danh hiệu {user.titleDonate}</color>\n");

            }
        }

        foreach (PlayerScore player in players)
        {
            if (player.titleWon == DataManager.ins.titleList[1] || player.titleWon == DataManager.ins.titleList[2] || player.titleWon == DataManager.ins.titleList[3] || player.titleWon == DataManager.ins.titleList[4] || player.titleWon == DataManager.ins.titleList[5] ||
                player.titleWon == DataManager.ins.titleList[6] || player.titleWon == DataManager.ins.titleList[7] || player.titleWon == DataManager.ins.titleList[8] || player.titleWon == DataManager.ins.titleList[9] || player.titleWon == DataManager.ins.titleList[10] ||
                player.titleWon == DataManager.ins.titleList[11] || player.titleWon == DataManager.ins.titleList[15] || player.titleWon == DataManager.ins.titleList[16] || player.titleWon == DataManager.ins.titleList[17] || player.titleWon == DataManager.ins.titleList[18])
            {
                Color chatColor = GetChatColor(null, null, null, player, null);

                chatStringBuilder.Append($"<color=#{ColorUtility.ToHtmlStringRGB(chatColor)}>{DateTime.Now.ToString("HH:mm")}  {player.playerName}: vừa đạt danh hiệu {player.titleWon}</color>\n");
            }
        }
    }
}