﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndGameUI : UIScreenBase
{
    public Text countDownTime;
    public Text[] playerNameTop3;
    public Text[] scoreTop3;
    public Text[] timeTop3;
    public Text[] playerNameTop8;
    public Text[] scoreTop8;
    public Text[] timeTop8;
    public Text congraTop1;


    void Start()
    {
        UpdateTimeText();
        UpdateCongratulationTop1();
        UpdateTop3RankPlayer();
        UpdateTop8RankPlayer();
    }

    void Update()
    {
        UpdateTimeText();
    }

    public void UpdateTimeText()
    {
        TimeSpan time = TimeSpan.FromSeconds(DataManager.ins.countDownEndGame);
        countDownTime.text = "Phiên tiếp theo sẽ bắt đầu sau " + time.Seconds.ToString() + "s nữa";
    }

    public void UpdateCongratulationTop1()
    {
        congraTop1.text = "Chúc mừng người chơi " + DataManager.ins.playerScores[1].playerName + "đã giành được chức vô địch";
        DataManager.ins.playerScores[1].winnerGame++;
    }

    public void UpdateTop3RankPlayer()
    {
        for (int i = 0; i < DataManager.ins.playerScores.Count; i++)
        {
            playerNameTop3[i].text = DataManager.ins.playerScores[i].playerName;
            scoreTop3[i].text = DataManager.ins.playerScores[i].score.ToString();
            timeTop3[i].text = GameController.ins.TimeFormatted(DataManager.ins.playerScores[i].timeToAnswer);
            if (i == 2)
            {
                break;
            }
        }
    }

    public void UpdateTop8RankPlayer()
    {
        for (int i = 0; i < DataManager.ins.playerScores.Count; i++)
        {
            playerNameTop8[i].text = DataManager.ins.playerScores[i].playerName;
            scoreTop8[i].text = DataManager.ins.playerScores[i].score.ToString();
            timeTop8[i].text = GameController.ins.TimeFormatted(DataManager.ins.playerScores[i].timeToAnswer);
            if (i == 7)
            {
                break;
            }
        }
    }

    public override void Hide()
    {
        base.Hide();
    }
    public override void OnShow()
    {
        base.OnShow();
    }
}
