using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static UIManager ins;
    [HideInInspector]
    public UIScreenBase currentScreen;
    [HideInInspector]
    public UIScreenBase currentScreen1;
    public GameObject windowUI;
    public GameObject popupUI;
    public GameObject startGameUI;
    public GameObject playGameUI;
    public GameObject endGameUI;
    public GameObject connectGameUI;
    public GameObject disconnectGameUI;
    public GameObject connectingGameUI;
    public GameObject uiCamera;

    private void Awake()
    {
        if(ins != null)
        {
            Destroy(gameObject);
        }
        else
        {
            ins = this;
            DontDestroyOnLoad(gameObject);
            DontDestroyOnLoad(uiCamera);
        }
    }

    private void Start()
    {
        Canvas canvas = gameObject.GetComponent<Canvas>();
        canvas.renderMode = RenderMode.ScreenSpaceCamera;
        canvas.worldCamera = uiCamera.GetComponent<Camera>();
        currentScreen = Instantiate(connectGameUI, windowUI.transform).GetComponent<ConnectUI>();
        currentScreen1 = Instantiate(disconnectGameUI, popupUI.transform).GetComponent<NotifyUI>();
    }

    public void ShowConnectGame()
    {
        currentScreen = Instantiate(connectGameUI, windowUI.transform).GetComponent<ConnectUI>();
    }

    public void ShowStarGame()
    {
        Destroy(currentScreen.gameObject);
        currentScreen = Instantiate(startGameUI, windowUI.transform).GetComponent<StartGameUI>();
        DataManager.ins.timeActiveStartGame = true;
    }

    public void ShowPlayGame()
    {
        Destroy(currentScreen.gameObject);
        currentScreen = Instantiate(playGameUI, windowUI.transform).GetComponent<PlayGameUI>();
        DataManager.ins.timeActivePlayGame = true;
    }

    public void ShowEndGame()
    {
        Destroy(currentScreen.gameObject);
        currentScreen = Instantiate(endGameUI, windowUI.transform).GetComponent<EndGameUI>();
        DataManager.ins.timeActiveEndGame = true;
    }

    public void ShowDisconnect()
    {
        Destroy(currentScreen1.gameObject);
        currentScreen1 = Instantiate(disconnectGameUI, popupUI.transform).GetComponent<NotifyUI>();
    }    

    public void ShowConnecting()
    {
        Destroy(currentScreen1.gameObject);
        currentScreen1 = Instantiate(connectingGameUI, popupUI.transform).GetComponent<NotifyUI>();
    }    
}
