using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NotifyUI : UIScreenBase
{
    public GameObject image;

    private void Start()
    {
        Notify();
    }

    public void Notify()
    {
        image.SetActive(true);     
    }

    public override void Hide()
    {
        base.Hide();
    }
    public override void OnShow()
    {
        base.OnShow();
    }
}
