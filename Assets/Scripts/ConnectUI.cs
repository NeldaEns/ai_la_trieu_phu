﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TikTokLiveUnity;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using DG.Tweening;
using TikTokLiveSharp.Client;
using System;
using System.Threading.Tasks;
using TikTokLiveSharp.Models.Protobuf.Messages;
using TikTokLiveSharp.Errors.Connections;
using TikTokLiveSharp.Errors.FetchErrors;

public class ConnectUI : UIScreenBase
{
    public string baseRegister = "http://27.72.102.114:7310/auth/signup";
    public string baseLogin = "http://27.72.102.114:7310/auth/login";

    [Header ("Login")]
    public InputField userNameLogin;
    public InputField passWordLogin;
    public GameObject loginObj;

    [Header("Register")]
    public InputField fullNameRegister;
    public InputField userNameRegister;
    public InputField passWordRegister;
    public GameObject registerObj;

    [Header ("Room")]
    public InputField hostName;
    public GameObject idRoom;
    public GameObject loadingObj;

    [Header("Message")]
    public GameObject errorLogObj;
    public Text errorLog;

    private void Start()
    {
        UpdateHostName();
    }

    public void UpdateHostName()
    {
        if(hostName != null)
        {
            TikTokLiveManager.Instance.autoConnectHostId = hostName.text;
        }
    }

    public void AccountRegister()
    {
        string uName = userNameRegister.text;
        string pWord = passWordRegister.text;
        string fName = fullNameRegister.text;
        if (string.IsNullOrWhiteSpace(uName) || string.IsNullOrWhiteSpace(pWord) || string.IsNullOrWhiteSpace(fName))
        {
            errorLogObj.SetActive(true);
            errorLog.text = "Please enter complete information";
            Sequence bannerSequence = DOTween.Sequence();
            bannerSequence.Append(errorLogObj.transform.DOScale(Vector3.one, 0.5f));
            bannerSequence.AppendInterval(1.0f);
            bannerSequence.Append(errorLogObj.transform.DOScale(Vector3.zero, 0.5f));
            bannerSequence.OnComplete(() => errorLogObj.SetActive(false));
        }
        else
        {
            StartCoroutine(RegisterNewAccount(uName, pWord, fName));
        }
    }

    public void AccountLogin()
    {
        string uName = userNameLogin.text;
        string pWord = passWordLogin.text;
        string gameClient = "qlWwcACyJDv5wZFHHyFoHcZmiJ6VPSOO";

        if(string.IsNullOrWhiteSpace(uName) || string.IsNullOrWhiteSpace(pWord))
        {
            errorLogObj.SetActive(true);
            errorLog.text = "Please fill in your full account and password";
            Sequence bannerSequence = DOTween.Sequence();
            bannerSequence.Append(errorLogObj.transform.DOScale(Vector3.one, 0.5f));
            bannerSequence.AppendInterval(1.0f);
            bannerSequence.Append(errorLogObj.transform.DOScale(Vector3.zero, 0.5f));
            bannerSequence.OnComplete(() => errorLogObj.SetActive(false));
        }    
        else
        {
        StartCoroutine(LoginAccount(uName, pWord, gameClient));
        }    
    }

    IEnumerator RegisterNewAccount(string uName, string pWord, string fName)
    {
        WWWForm form = new WWWForm();
        form.AddField("username", uName);
        form.AddField("password", pWord);
        form.AddField("fullName", fName);
        using (UnityWebRequest www = UnityWebRequest.Post(baseRegister, form)) 
        {
            www.downloadHandler = new DownloadHandlerBuffer();
            yield return www.SendWebRequest();

            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                Debug.Log(www.error);
                errorLogObj.SetActive(true);
                errorLog.text = "Unable to connect to server";
                Sequence bannerSequence = DOTween.Sequence();
                bannerSequence.Append(errorLogObj.transform.DOScale(Vector3.one, 0.5f));
                bannerSequence.AppendInterval(1.0f);
                bannerSequence.Append(errorLogObj.transform.DOScale(Vector3.zero, 0.5f));
                bannerSequence.OnComplete(() => errorLogObj.SetActive(false));
            }

            else
            {
                string responseText = www.downloadHandler.text;
                ServerReponseRegister serverReponse = JsonUtility.FromJson<ServerReponseRegister>(responseText);
                if (responseText != null)
                {
                    if (serverReponse.code == "404")
                    {
                        errorLogObj.SetActive(true);
                        errorLog.text = "Account already exists";
                        Sequence bannerSequence = DOTween.Sequence();
                        bannerSequence.Append(errorLogObj.transform.DOScale(Vector3.one, 0.5f));
                        bannerSequence.AppendInterval(1.0f);
                        bannerSequence.Append(errorLogObj.transform.DOScale(Vector3.zero, 0.2f));
                        bannerSequence.OnComplete(() => errorLogObj.SetActive(false));
                    }
                    else
                    {
                        errorLogObj.SetActive(true);
                        errorLog.text = "SUCCESSFUL!";
                        Sequence bannerSequence = DOTween.Sequence();
                        bannerSequence.Append(errorLogObj.transform.DOScale(Vector3.one, 0.5f));
                        bannerSequence.AppendInterval(0.01f);
                        //bannerSequence.Append(errorLogObj.transform.DOScale(Vector3.zero, 0.2f));
                        bannerSequence.OnComplete(() => errorLogObj.SetActive(false));
                        loginObj.SetActive(true);
                        registerObj.SetActive(false);
                    }
                }
            }
        }
    }

    IEnumerator LoginAccount(string uName, string pWord, string gameClient)
    {
        WWWForm form = new WWWForm();
        form.AddField("gameClient", gameClient);
        form.AddField("username", uName);
        form.AddField("password", pWord);
        using (UnityWebRequest www = UnityWebRequest.Post(baseLogin, form))
        {
            www.downloadHandler = new DownloadHandlerBuffer();
            yield return www.SendWebRequest();

            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                Debug.Log(www.error);
                errorLogObj.SetActive(true);
                errorLog.text = "Unable to connect to server";
                Sequence bannerSequence = DOTween.Sequence();
                bannerSequence.Append(errorLogObj.transform.DOScale(Vector3.one, 0.5f));
                bannerSequence.AppendInterval(1.0f);
                bannerSequence.Append(errorLogObj.transform.DOScale(Vector3.zero, 0.5f));
                bannerSequence.OnComplete(() => errorLogObj.SetActive(false));
            }
            else
            {
                string responseText = www.downloadHandler.text;
                ServerReponseRegister serverReponse = JsonUtility.FromJson<ServerReponseRegister>(responseText);
                if (responseText != null)
                {
                    if (serverReponse.code == "404")
                    {
                        errorLogObj.SetActive(true);
                        errorLog.text = "Username or password is wrong";
                        Sequence bannerSequence = DOTween.Sequence();
                        bannerSequence.Append(errorLogObj.transform.DOScale(Vector3.one, 0.5f));
                        bannerSequence.AppendInterval(1.0f);
                        bannerSequence.Append(errorLogObj.transform.DOScale(Vector3.zero, 0.2f));
                        bannerSequence.OnComplete(() => errorLogObj.SetActive(false));
                    }
                    else
                    {
                        idRoom.SetActive(true);
                        loginObj.SetActive(false);
                    }
                }
            }
        }
    }

    public async void ConnectToStreamAsync()
    {
        if (string.IsNullOrWhiteSpace(hostName.text))
        {
            errorLogObj.SetActive(true);
            errorLog.text = "Please enter Room ID";
            Sequence bannerSequence = DOTween.Sequence();
            bannerSequence.Append(errorLogObj.transform.DOScale(Vector3.one, 0.5f));
            bannerSequence.AppendInterval(1.0f);
            bannerSequence.Append(errorLogObj.transform.DOScale(Vector3.zero, 0.2f));
            bannerSequence.OnComplete(() => errorLogObj.SetActive(false));
        }
        else
        {
            DataManager.ins.roomId = hostName.text;
            loadingObj.SetActive(true);
            idRoom.SetActive(false);
            await TikTokLiveManager.Instance.ConnectToStream(hostName.text, (Exception ex) => {
                if (ex is LiveNotFoundException)
                {
                    errorLogObj.SetActive(true);
                    errorLog.text = "Could not find room. \n LiveStream for HostID could not be found. Is the Host online?";
                    Sequence bannerSequence = DOTween.Sequence();
                    bannerSequence.Append(errorLogObj.transform.DOScale(Vector3.one, 0.5f));
                    bannerSequence.AppendInterval(1.0f);
                    bannerSequence.Append(errorLogObj.transform.DOScale(Vector3.zero, 0.2f));
                    bannerSequence.OnComplete(() => errorLogObj.SetActive(false));
                    loadingObj.SetActive(false);
                    idRoom.SetActive(true);
                }    
                else if(ex is FailedFetchRoomInfoException)
                {
                    errorLogObj.SetActive(true);
                    errorLog.text = "User might be offline. \n or Your IP or country might be blocked by TikTok";
                    Sequence bannerSequence = DOTween.Sequence();
                    bannerSequence.Append(errorLogObj.transform.DOScale(Vector3.one, 0.5f));
                    bannerSequence.AppendInterval(1.0f);
                    bannerSequence.Append(errorLogObj.transform.DOScale(Vector3.zero, 0.2f));
                    bannerSequence.OnComplete(() => errorLogObj.SetActive(false));
                    loadingObj.SetActive(false);
                    idRoom.SetActive(true);
                }
            });    
        }
    }

    public void LoginBtn()
    {
        loginObj.SetActive(true);
        registerObj.SetActive(false);
    }    

    public void RegisterBtn()
    {
        registerObj.SetActive(true);
        loginObj.SetActive(false);
    }

    public override void Hide()
    {
        base.Hide();
    }
    public override void OnShow()
    {
        base.OnShow();
    }

    [System.Serializable]
    public class ServerReponseRegister
    {
        public string code;
        public string message;
        public ServerReponseLogin data;
    }

    public class ServerReponseLogin
    {
        public DataUser user;
        public string token;
    }

    public class DataUser
    {
        public string ID { get; set; }
        public string accountID { get; set; }
        public string fullName { get; set; }
        public string tiktokID { get; set; }
        public string sessionID { get; set; }
        public string createdAt { get; set; }
        public string updatedAt { get; set; }
    }
}
