﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TikTokLiveSharp.Client;
using TikTokLiveSharp.Events;
using TikTokLiveSharp.Events.Objects;
using TikTokLiveUnity;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TikTokController : MonoBehaviour
{
    public bool firstConnect = false;

    private void Start()
    {
        TikTokLiveManager.Instance.OnConnected += OnConnected;
        TikTokLiveManager.Instance.OnChatMessage += OnChatReceive;
        TikTokLiveManager.Instance.OnGiftMessage += OnGiftReceive;
        TikTokLiveManager.Instance.OnGift += OnGiftReceive1;
    }

    public void OnChatReceive(TikTokLiveClient sender, Chat val)
    {
        Debug.Log(val.Message);
        if (val.Message == "Go")
        {
            Debug.Log("Go");
            if (DataManager.ins.countDownStartGame > 0)
            {
                if (DataManager.ins.playerCount.Count == 0)
                {
                    DataManager.ins.playerCount.Add(val.Sender);
                }
                else
                {
                    if (!DataManager.ins.playerCount.Any(player => player.Id == val.Sender.Id))
                    {
                        DataManager.ins.playerCount.Add(val.Sender);
                    }
                }
            }
        }
        if (DataManager.ins.timeActivePlayGame && DataManager.ins.countDownPlayGame > 5)
        {
            var playerId = val.Sender.Id;

            bool isPlayerInGame = DataManager.ins.playerCount.Any(player => player.Id == playerId);
            bool isAnswerComment = new[] { "A", "B", "C", "D" }.Contains(val.Message);

            if (isPlayerInGame && isAnswerComment)
            {
                string chosenAnswer = null;
                List<List<User>> answerLists = new List<List<User>>
                {
                    DataManager.ins.ansAPlayerCount,
                    DataManager.ins.ansBPlayerCount,
                    DataManager.ins.ansCPlayerCount,
                    DataManager.ins.ansDPlayerCount
                };

                List<User> previousList = null;

                foreach (var list in answerLists)
                {
                    if (list.Any(player => player.Id == playerId))
                    {
                        previousList = list;
                        break;
                    }
                }

                if (previousList != null)
                {
                    previousList.RemoveAll(player => player.Id == playerId);
                }

                if (isAnswerComment)
                {
                    chosenAnswer = (val.Message == "A") ? "0" :
                                   (val.Message == "B") ? "1" :
                                   (val.Message == "C") ? "2" :
                                   (val.Message == "D") ? "3" : null;
                }
                else
                {
                    chosenAnswer = val.Message;
                }
                if (!string.IsNullOrEmpty(chosenAnswer))
                {
                    PlayerScore playerScore = new PlayerScore();
                    playerScore.UpdateLastChosenAnswer(chosenAnswer);
                    switch (val.Message)
                    {
                        case "A":
                            DataManager.ins.ansAPlayerCount.Add(val.Sender);
                            break;
                        case "B":
                            DataManager.ins.ansBPlayerCount.Add(val.Sender);
                            break;
                        case "C":
                            DataManager.ins.ansCPlayerCount.Add(val.Sender);
                            break;
                        case "D":
                            DataManager.ins.ansDPlayerCount.Add(val.Sender);
                            break;
                        default:
                            break;
                    }
                    float timeToAnswer = 60.0f - DataManager.ins.countDownPlayGame;

                    GameController.ins.TimeFormatted(timeToAnswer);
                    GameController.ins.PlayerAnswered(playerId, val.Sender.NickName, chosenAnswer, DataManager.ins.correctAnswer.ToString(), timeToAnswer);
                }
            }
        }
        if (DataManager.ins.userDonateCount.Count > 0)
        {
            bool isUserDonate = DataManager.ins.userDonateCount.Any(user => user.userId == val.Sender.Id);
            if (isUserDonate)
            {
                DataManager.ins.chatUserDonate.Add(val);
            }
        }
    }

    public void OnGiftReceive(TikTokLiveClient sender, GiftMessage val)
    {
        if (val.StreakEnd)
        {
            
            Debug.LogError(val.User.NickName + " " + val.Gift.Name + "  Amount" + val.Amount);
            DataManager.ins.userGift.Add(val);
            var userDonate = DataManager.ins.userDonateCount.FirstOrDefault(user => user.userId == val.User.Id);
            if (userDonate == null)
            {
                userDonate = new UserDonateGift
                {
                    userId = val.User.Id,
                    userName = val.User.NickName,
                    rose = 0,
                    gg = 0,
                    tiktok = 0,
                    donut = 0,
                    fingerHeart = 0,
                    rosa = 0,
                    perfume = 0,
                    numberOfDonations = 1,
                };
                DataManager.ins.userDonateCount.Add(userDonate);
            }
            else
            {
                userDonate.numberOfDonations++;
            }

            switch (val.Gift.Id)
            {
                case 5655:
                    userDonate.rose += val.Amount;
                    break;
                case 5269:
                    userDonate.tiktok += val.Amount;
                    break;
                case 5487:
                    userDonate.fingerHeart += val.Amount;
                    break;
                case 6064:
                    userDonate.gg += val.Amount;
                    break;
                case 8913:
                    userDonate.rosa += val.Amount;
                    break;
                case 5658:
                    userDonate.perfume += val.Amount;
                    break;
                case 5879:
                    userDonate.donut += val.Amount;
                    break;
            }

            DataManager.ins.giftColors = new Dictionary<long, Color>
        {
            {5655, Color.green },
            {5269, Color.blue },
            {5487, new Color(251/255f, 115/255f, 255/255f) },
            {6064, Color.yellow },
            {8913, new Color(242/255f, 177/255f, 61/255f) },
            {5658, new Color(251/255f, 115/255f, 255/255f) },
            {5879, new Color(243 / 255f, 255 / 255f, 160 / 255f) },
        };
        }
    }

    public void OnGiftReceive1(TikTokLiveClient sender, TikTokGift val)
    {
        if (val.StreakFinished)
        {
            Debug.LogError(val.Sender.NickName + " " + val.Gift.Name + "  Amount" + val.Amount);
            foreach (GiftMessage gift in DataManager.ins.userGift)
            {
                if (DataManager.ins.userGift1.Count == 0)
                {
                    if (val.Sender.Id != gift.User.Id)
                    {
                        DataManager.ins.userGift1.Add(val);
                    }
                }
                else if (DataManager.ins.userGift1.Count > 0)
                {
                    if (val.Sender.Id != gift.User.Id)
                    {
                        if (!DataManager.ins.userGift1.Any(player => player.Sender.Id == val.Sender.Id))
                        {
                            DataManager.ins.userGift1.Add(val);
                        }
                    }
                }
            }
            var userDonate = DataManager.ins.userDonateCount.FirstOrDefault(user => user.userId == val.Sender.Id);
            if (userDonate == null)
            {
                userDonate = new UserDonateGift
                {
                    userId = val.Sender.Id,
                    userName = val.Sender.NickName,
                    rose = 0,
                    gg = 0,
                    tiktok = 0,
                    donut = 0,
                    fingerHeart = 0,
                    rosa = 0,
                    perfume = 0,
                    numberOfDonations = 1,
                };
                DataManager.ins.userDonateCount.Add(userDonate);
            }
            else
            {
                userDonate.numberOfDonations++;
            }
            switch (val.Gift.Id)
            {
                case 5655:
                    userDonate.rose += val.Amount;
                    break;
                case 5269:
                    userDonate.tiktok += val.Amount;
                    break;
                case 5487:
                    userDonate.fingerHeart += val.Amount;
                    break;
                case 6064:
                    userDonate.gg += val.Amount;
                    break;
                case 8913:
                    userDonate.rosa += val.Amount;
                    break;
                case 5658:
                    userDonate.perfume += val.Amount;
                    break;
                case 5879:
                    userDonate.donut += val.Amount;
                    break;
            }

            DataManager.ins.giftColors = new Dictionary<long, Color>
        {
            {5655, Color.green },
            {5269, Color.blue },
            {5487, new Color(251/255f, 115/255f, 255/255f) },
            {6064, Color.yellow },
            {8913, new Color(242/255f, 177/255f, 61/255f) },
            {5658, new Color(251/255f, 115/255f, 255/255f) },
            {5879, new Color(243 / 255f, 255 / 255f, 160 / 255f) },
        };

        }
    }

    public void OnConnected(TikTokLiveClient sender, bool val)
    {
        if (Application.internetReachability != NetworkReachability.NotReachable)
        {
            SceneManager.LoadScene(1);
            ((ConnectUI)UIManager.ins.currentScreen).loadingObj.SetActive(false);
            UIManager.ins.ShowStarGame();
            DataManager.ins.timeActiveStartGame = true;
            firstConnect = true;
        }
        else
        {
            ((ConnectUI)UIManager.ins.currentScreen).errorLogObj.SetActive(true);
            ((ConnectUI)UIManager.ins.currentScreen).errorLog.text = "Unable to connect to server";
            Sequence bannerSequence = DOTween.Sequence();
            bannerSequence.Append(((ConnectUI)UIManager.ins.currentScreen).errorLogObj.transform.DOScale(Vector3.one, 0.5f));
            bannerSequence.AppendInterval(1.0f);
            bannerSequence.Append(((ConnectUI)UIManager.ins.currentScreen).errorLogObj.transform.DOScale(Vector3.zero, 0.5f));
            bannerSequence.OnComplete(() => ((ConnectUI)UIManager.ins.currentScreen).errorLogObj.SetActive(false));
        }
    }

    private async void Update()
    {
        if (firstConnect)
        {
            if (!TikTokLiveManager.Instance.Connected && !TikTokLiveManager.Instance.Connecting)
            { 
                UIManager.ins.popupUI.SetActive(true);
                UIManager.ins.ShowDisconnect();
                if (Application.internetReachability != NetworkReachability.NotReachable)
                    await TikTokLiveManager.Instance.ConnectToStream(DataManager.ins.roomId);
            }
            else if (!TikTokLiveManager.Instance.Connected && TikTokLiveManager.Instance.Connecting)
            {
                UIManager.ins.popupUI.SetActive(true);
                UIManager.ins.ShowConnecting();
            }
            else if (TikTokLiveManager.Instance.Connected)
            {
                UIManager.ins.popupUI.SetActive(false);
            }
        }
    }
}

