﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using TikTokLiveUnity;
using TikTokLiveSharp;

public class StartGameUI : UIScreenBase
{
    public Text viewerText;
    public Text playerText;
    public Text countDownTime;
    public GameObject clock;

    void Start()
    {
        UpdateTimeText();
        UpdateViewerText();
        UpdatePlayerText();
    }

    void Update()
    {
        UpdateTimeText();
        UpdateViewerText();
        UpdatePlayerText();
    }

    public void UpdateTimeText()
    {
        TimeSpan time = TimeSpan.FromSeconds(DataManager.ins.countDownStartGame);
        countDownTime.text = time.Minutes.ToString() + ":" + time.Seconds.ToString();
        clock.GetComponent<Slider>().value -= Time.deltaTime;
    }

    public void UpdateViewerText()
    {
        viewerText.text = TikTokLiveManager.Instance.ViewerCount.ToString();
    }

    public void UpdatePlayerText()
    {
        playerText.text = DataManager.ins.playerCount.Count.ToString();
    }

    public override void Hide()
    {
        base.Hide();
    }
    public override void OnShow()
    {
        base.OnShow();
    }
}
