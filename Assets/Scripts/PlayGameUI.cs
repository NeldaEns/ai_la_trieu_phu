﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using TikTokLiveSharp.Events;
using Random = UnityEngine.Random;
using TikTokLiveUnity;

public class PlayGameUI : UIScreenBase
{
   [Header ("QUESTION")]
    public Text questTxt;
    public Text[] answerTxts;
    public GameObject[] answerObj;
    public Text questionsNumber;
    public Text ansACount;
    public Text ansBCount;
    public Text ansCCount;
    public Text ansDCount;

    [Header("BXH")]
    public Text[] playerName;
    public Text[] score;
    public Text[] time;
    public GameObject bxhTop3;

    [Header("DONATE")]
    public Text donateNotify;
     
    [Header("TIME")]
    public Text countDownTime;
    public GameObject clock;

    void Start()
    {
        UpdateTimeText();
        UpdateQuestTxt();
        UpdateAnsACount();
        UpdateAnsBCount();
        UpdateAnsCCount();
        UpdateAnsDCount();
        UpdateRankPlayer();
        UpdateDonateGift();
    }

    void Update()
    {
        UpdateDonateGift();
        UpdateTimeText();
        UpdateAnsACount();
        UpdateAnsBCount();
        UpdateAnsCCount();
        UpdateAnsDCount();
        UpdateRankPlayer();
    }
    public void UpdateTimeText()
    {
        TimeSpan time = TimeSpan.FromSeconds(DataManager.ins.countDownPlayGame);
        countDownTime.text = time.Minutes.ToString() + ":" + time.Seconds.ToString();
        clock.GetComponent<Slider>().value -= Time.deltaTime;
    }

    public void UpdateQuestTxt()
    {
        if (DataManager.ins.questionsData.Count > 0)
        {
            questionsNumber.text = "Câu hỏi số " + DataManager.ins.questionNumber.ToString() + ":";
            int randomQuestIndex = Random.Range(0, DataManager.ins.questionsData.Count);
            QuestionData randomQuest = DataManager.ins.questionsData[randomQuestIndex];
            DataManager.ins.correctAnswer = randomQuest.correctAnswer;
            questTxt.text = randomQuest.questionText;
            for (int i = 0; i < randomQuest.answers.Length; i++)
            {
                answerObj[i].GetComponent<Image>().color = Color.cyan;
                answerTxts[i].text = randomQuest.answers[i];
            }
            DataManager.ins.questionsData.RemoveAt(randomQuestIndex);
        }
    }  

    public void UpdateAnsACount()
    {
        ansACount.text = string.Format("{0:F1}%", ((float)DataManager.ins.ansAPlayerCount.Count / (float)DataManager.ins.playerCount.Count) * 100);
    }

    public void UpdateAnsBCount()
    {
        ansBCount.text = string.Format("{0:F1}%", ((float)DataManager.ins.ansBPlayerCount.Count / (float)DataManager.ins.playerCount.Count) * 100);
    }

    public void UpdateAnsCCount()
    {
        ansCCount.text = string.Format("{0:F1}%", ((float)DataManager.ins.ansCPlayerCount.Count / (float)DataManager.ins.playerCount.Count) * 100);
    }

    public void UpdateAnsDCount()
    {
        ansDCount.text = string.Format("{0:F1}%", ((float)DataManager.ins.ansDPlayerCount.Count / (float)DataManager.ins.playerCount.Count) * 100);
    }

    public void UpdateRankPlayer()
    {
        for (int i = 0; i < DataManager.ins.playerScores.Count; i++)
        {
            playerName[i].text = DataManager.ins.playerScores[i].playerName;
            score[i].text = DataManager.ins.playerScores[i].score.ToString();
            time[i].text = GameController.ins.TimeFormatted(DataManager.ins.playerScores[i].timeToAnswer);
            if (i == 2)
            {
                break;
            }
        }
    }

    public void UpdateDonateGift()
    {
        if (DataManager.ins.userGift.Count > 0)
        {
            foreach (GiftMessage user in DataManager.ins.userGift)
            {
                donateNotify.text = "Người chơi " + user.User.NickName + " vừa donate " + user.Amount + " " + user.Gift.Name;               
            }
        }
    }

    public override void Hide()
    {
        base.Hide();
    }
    public override void OnShow()
    {
        base.OnShow();
    }
}
